const gulp = require('gulp'),
  sourcemaps = require('gulp-sourcemaps')
  replace = require('gulp-replace')
  concat = require('gulp-concat-css');

gulp.task('css', () => {
  return gulp.src('src/css/theme.css')
    .pipe(sourcemaps.init())
    .pipe(concat('theme.css.liquid'))
    .pipe(sourcemaps.write('maps'))
    .pipe(replace('"{{', '{{'))
    .pipe(replace('}}"', '}}'))
    .pipe(replace('"{%', '{%'))
    .pipe(replace('%}"', '%}'))
    .pipe(gulp.dest('assets'));
});

gulp.task('watch', () => {
  gulp.watch('src/css/**/*.css', gulp.series('css'));
});

exports.default = () => {
  gulp.series('css');
};
