# BVA Challenge

Custom Shopify theme for the BVA code challenge.

https://bva-challenge.myshopify.com


### Dev prerequesites
1. [Themekit](https://github.com/Shopify/themekit)
2. [Nodejs](https://nodejs.org/en/)/npm
3. [gulp-cli](https://www.npmjs.com/package/gulp-cli)

### Dev setup
1. Duplicate the theme in the store admin
2. Configure `themekit` to use the duplicate theme
3. Install `gulp-cli` globally (`npm i -g gulp-cli`)
4. Install dev tooling (`npm i`)
5. Watch for CSS changes (`gulp watch`)
6. Watch for theme changes (`theme watch`)
